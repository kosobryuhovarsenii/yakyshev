$(document).ready(function(){

  $(window).resize(function () {
    $(".viewport").attr("content", "width=" + (window.screen.width >= 480 ? "1250" : "device-width"));
  });

  $('.slider-photo-1').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    swipe: false,
    asNavFor: '.slider-photo-2'
  });
  $('.slider-photo-2').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: true,
    autoplay: true,
    arrows: false,
    swipe: false,
    asNavFor: '.slider-photo-1',
    responsive: [
      {
        breakpoint: 479,
        settings: {
          slidesToShow: 1,
          swipe: true,
        }
      }
    ]
  });
  $('.reviews-slider').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
    autoplay: true,
    responsive: [
      {
        breakpoint: 479,
        settings: {
          slidesToShow: 1,
          swipe: true,
        }
      }
    ]
  })
  $('.products-block').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
    autoplay: false,
    responsive: [
      {
        breakpoint: 479,
        settings: {
          slidesToShow: 1,
          swipe: true,
        }
      }
    ]
  })
  $('.sert-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
    autoplay: true,
    responsive: [
      {
        breakpoint: 479,
        settings: {
          slidesToShow: 1,
          swipe: true,
        }
      }
    ]
  })

  $(".header-menu li a").click(function (e) {
    event.preventDefault(e);
    list(this);
  });

  $(".footer-menu li a").click(function (e) {
    event.preventDefault(e);
    list(this);
  });

  $(".footer-logo").click(function (e) {
    event.preventDefault(e);
    list(this);
  });

  $('.button').click(function (e) {
    list(this);
  });

  function list (el) {
    var elementClick = $(el).attr("href");
    var destination = $(elementClick).offset().top;
    $("html:not(:animated),body:not(:animated)").animate({
      scrollTop: destination
    }, 1000);
    return false;
  }

  $('.header-menu__button').on('click', function(){
    $('.header-block').slideToggle(400);
    $('.header').toggleClass('header-active')
    
    $(".header-menu li a").click(function (e) {
      event.preventDefault(e);
      $('.header-block').slideUp(300)
      list(this);
    });
  })

  //аккордеон//
  $('.faq-block__item h3').on('click', func);
  function func(){
    // $('.faq-block__item-desc').not($(this).next()).slideUp(300);
    $(this).toggleClass('in').next().slideToggle(300);
    // $('.faq-block__item h3').not(this).removeClass('in');
  }

  
});